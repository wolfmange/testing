#include <iostream>
#include <string>

#include "Radnik.hpp"
#include "Vozac.hpp"
#include "Prodavac.hpp"
#include "Magacioner.hpp"
#include "Odeljenje.hpp"
#include "Preduzece.hpp"


ostream &operator<<(ostream &output, Radnik *radnik){
    radnik->ispis(output);
    return output;
}

istream &operator>>(istream &input, Radnik *radnik){

    radnik->ucitaj(input);
    return input;
}

ostream &operator<<(ostream &output, Odeljenje *odeljenje)
{
 
    odeljenje->ispis(output);
    return output;
}

ostream &operator<<(ostream &output, Preduzece *preduzece){
    preduzece->ispis(output);
    return output;
}


int main(){

    Radnik *p = new Prodavac("Jovan", "Jovanovic", 2000, 23, nullptr);
    Radnik *p2 = new Prodavac("Petar", "Petrovic", 2000, 394, nullptr);
    Radnik *v2 = new Vozac("Luka", "Lukic", 2000, 2, p);
    Radnik *v3 = new Vozac("Teodora", "Teodorovic", 3000, 2, p2);
    Radnik *m = new Magacioner("Jelena", "Jelenovic", 2000, nullptr);


    Odeljenje *o = new Odeljenje("o1", p);
    Odeljenje *o2 = new Odeljenje("o2", p2);

    o->zaposliti(p);
    o->zaposliti(v2);
    o->zaposliti(p2);
    o2->zaposliti(v3);
    o2->zaposliti(m);

    Preduzece *pr = new Preduzece("NazivPreduzeca", 1232, 348);
    pr->dodajOdeljenje(o);
    pr->dodajOdeljenje(o2);

    // ofstream output("info.txt");
    // output << pr << endl;


    // output.close();
    // cout << o;
    // cout << p;
    // Radnik *r = new Vozac();
    // ifstream input("info.txt");
    // input >> r;
    // input.close();

    // cout << r << endl;
    
    // delete pr;

    return 0;
};