#include "Magacioner.hpp"

    Magacioner::Magacioner() : Radnik(){}

    Magacioner::Magacioner(string ime, string prezime, double visinaPlate, Radnik *nadredjeni) : Radnik(ime, prezime, visinaPlate, nadredjeni){}

    string Magacioner::tip()
    {
        return "[MAGACIONER]";
    }

    void Magacioner::ispis(ostream &output) const 
    {
        Radnik::ispis(output);
        output << endl << endl;
    }
    
    void Magacioner::ucitaj(istream &input)
    {
        Radnik::ucitaj(input);
     
    }

    Magacioner::~Magacioner()
    {
        cout << "Unisten magacioner: " << Radnik::getIme() << endl;
    }