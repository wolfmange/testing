#ifndef PREDUZECE_HPP
#define PREDUZECE_HPP

#include <iostream>
#include <string>
#include <vector>
#include<fstream>

#include "Odeljenje.hpp"

using namespace std;
class Preduzece{

    private:
        string naziv;
        int maticniBroj;
        int pib;
        vector<Odeljenje *> odeljenja;
    
    public:
        Preduzece();
        Preduzece(string naziv, int maticniBroj, int pib);
        string getNaziv() const;
        void setNaziv(string naziv);
        int getMaticniBroj() const;
        void setMaticniBroj(int maticniBroj);
        int getPib() const;
        void setPib(int pib);
        vector<Odeljenje *> getOdeljenja() const;
        void setOdeljenja(vector<Odeljenje *> odeljenja);
        void dodajOdeljenje(Odeljenje *o);
        Odeljenje* dobaviOdeljenje(int i);
        void brisanje();
        string tip();
        void ispis(ostream &output) const;
        virtual ~Preduzece();
};
#endif