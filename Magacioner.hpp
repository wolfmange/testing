#ifndef MAGACIONER_HPP
#define MAGACIONER_HPP

#include "Radnik.hpp"

class Magacioner : public Radnik{

    public:
        Magacioner();
        Magacioner(string ime, string prezime, double visinaPlate, Radnik *nadredjeni);
        string tip();
        void ispis(ostream &output) const;
        void ucitaj(istream &input);
        virtual ~Magacioner();
};
#endif