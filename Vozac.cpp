#include "Vozac.hpp"

    Vozac::Vozac(){}

    Vozac::Vozac(string ime, string prezime, double visinaPlate, int brojPrekrsaja, Radnik *nadredjeni) : 
                                                                                    Radnik(ime, prezime, visinaPlate, nadredjeni),
                                                                                    brojPrekrsaja(brojPrekrsaja){}

    int Vozac::getBrojPrekrsaja() const
    {
        return brojPrekrsaja;
    }
    void Vozac::setBrojPrekrsaja(int brojPrekrsaja)
    {
        this->brojPrekrsaja = brojPrekrsaja;
    }
    void Vozac::dodajKategoriju(string kategorija)
    {
        kategorije.push_back(kategorija);
        
    }

    string Vozac::tip()
    {
        return "[VOZAC]";
    }

    void Vozac::ispis(ostream &output) const
    {

        Radnik::ispis(output);
        output << brojPrekrsaja << endl << endl;
    }

    void Vozac::ucitaj(istream &input)
    {
        Radnik::ucitaj(input);
        input >> brojPrekrsaja;       
    }

    Vozac::~Vozac(){

        cout << "Unisten vozac: " << Radnik::getIme() << endl;
        
    }