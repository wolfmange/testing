#include "Radnik.hpp"

    Radnik::Radnik(){}

    Radnik::Radnik(string ime, string prezime, double visinaPlate, Radnik *nadredjeni) : ime(ime),
                                                                                         prezime(prezime),
                                                                                         visinaPlate(visinaPlate),
                                                                                         nadredjeni(nadredjeni){

                                                                                             
                                                                                         }


    string Radnik::getIme() const
    {
        return ime;
    }
    void Radnik::setIme(string ime)
    {
        this->ime = ime;
    }
    string Radnik::getPrezime() const
    {
        return prezime;
    }
    void Radnik::setPrezime(string prezime)
    {
        this->prezime = prezime;
    }
    double Radnik::getVisinaPlate() const
    {
        return visinaPlate;
    }
    void Radnik::setVisinaPlate(double visinaPlate)
    {
        this->visinaPlate = visinaPlate;
    }
    Radnik* Radnik::getNadredjeni() const
    {
        return nadredjeni;
    }
    void Radnik::setNadredjeni(Radnik *nadredjeni)
    {
        this->nadredjeni = nadredjeni;
    }

    string Radnik::tip()
    {
        return "[RADNIK]";
    }
    void Radnik::ispis(ostream &output) const
    {
        output << ime << " " << prezime << " " << visinaPlate << " ";

    }
    void Radnik::ucitaj(istream &input)
    {
        
    
        input >> ime >> prezime >> visinaPlate;
    }
    Radnik::~Radnik(){
        
    }