#include "Odeljenje.hpp"

    Odeljenje::Odeljenje(){}

    Odeljenje::Odeljenje(string naziv, Radnik *sefOdeljenja) : naziv(naziv), sefOdeljenja(sefOdeljenja)
                                                                                            
                                                                                            {

        // cout << "Kreirano odeljenje." << endl;
    }

    string Odeljenje::getNaziv() const
    {
        return naziv;
    }
    void Odeljenje::setNaziv(string naziv)
    {
        this->naziv = naziv;
    }
    Radnik* Odeljenje::getSefOdeljenja()
    {
        return sefOdeljenja;
    }
    void Odeljenje::setSefOdeljenja(Radnik *sefOdeljenja)
    {
        this->sefOdeljenja = sefOdeljenja;
    }
    vector<Radnik *> Odeljenje::getZaposleni() const
    {
        return zaposleni;
    }
    void Odeljenje::setZaposleni(vector<Radnik *> zaposleni)
    {
        this->zaposleni = zaposleni;
    }

    void Odeljenje::zaposliti(Radnik *radnik)
    {
        zaposleni.push_back(radnik);
    }
    void Odeljenje::datiOtkaz(int indeks)
    {
        if(zaposleni.size() == 0 || indeks >= zaposleni.size()){
            throw out_of_range ("Nepostojeci clan.");
            return;
        }
        zaposleni.erase(zaposleni.begin()+indeks);
    }

    Radnik* Odeljenje::dobaviZaposlenog(int indeks)
    {
        return zaposleni.at(indeks);
    }

    string Odeljenje::tip(){
        return "[ODELJENJE]";
    }

    void Odeljenje::ispis(ostream &output) const
    {
        output << "[ODELJENJE]" << endl << endl;
        output << naziv << endl << endl;
        output << zaposleni.size() << endl;
        output << "[VOZAC]" << endl << endl;

        for(Radnik *r : zaposleni)
            {   
                
                if(r->tip() == "[VOZAC]"){
                    
                    r->ispis(output);
                }
                
            }
        output << "[PRODAVAC]" << endl << endl;
        for(Radnik *r : zaposleni)
            {   
                
                if(r->tip() == "[PRODAVAC]"){
                    
                    r->ispis(output);
                }
                
            }
        output << "[MAGACIONER]" << endl << endl;
        for(Radnik *r : zaposleni)
            {   
                
                if(r->tip() == "[MAGACIONER]"){
                    
                    r->ispis(output);
                }
                
            }
        
    }

    void Odeljenje::ucitaj(istream &input)
    {
        int velicina;
        input >> velicina;
        for(int i=0; i < velicina; i++){
            //
        }
    }
    Odeljenje::~Odeljenje(){
        
        for(Radnik *r : zaposleni)
            delete r;
        
        zaposleni.clear();
        // delete sefOdeljenja;
        
        
        cout << "Unisteno odeljenje " << naziv << endl;
    }