#ifndef ODELJENJE_HPP
#define ODELJENJE_HPP

#include <iostream>
#include <string>
#include <vector>
#include<fstream>

#include "Radnik.hpp"

using namespace std;
class Odeljenje{

    private:
        string naziv;
        Radnik *sefOdeljenja;                          
        vector<Radnik *> zaposleni;
    
    public:
        Odeljenje();
        Odeljenje(string naziv, Radnik *sefOdeljenja);
        string getNaziv() const;
        void setNaziv(string naziv);
        Radnik* getSefOdeljenja();
        void setSefOdeljenja(Radnik *sefOdeljenja);
        vector<Radnik *> getZaposleni() const;
        void setZaposleni(vector<Radnik *> zaposleni);
        void zaposliti(Radnik *radnik);
        void datiOtkaz(int indeks);
        Radnik * dobaviZaposlenog(int indeks);
        string tip();
        void ispis(ostream &output) const;
        void ucitaj(istream &input);
        virtual ~Odeljenje();

};
#endif