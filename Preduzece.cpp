#include "Preduzece.hpp"

    Preduzece::Preduzece(){}

    Preduzece::Preduzece(string naziv, int maticniBroj, int pib) : naziv(naziv), 
                                                                   maticniBroj(maticniBroj), 
                                                                   pib(pib)
                                                                {}
                                    
    string Preduzece::getNaziv() const
    {
        return naziv;
    }
    void Preduzece::setNaziv(string naziv)
    {
        this->naziv = naziv;
    }
    int Preduzece::getMaticniBroj() const
    {
        return maticniBroj;
    }
    void Preduzece::setMaticniBroj(int maticniBroj)
    {
        this->maticniBroj = maticniBroj;
    }
    int Preduzece::getPib() const
    {
        return pib;
    }
    void Preduzece::setPib(int pib)
    {
        this->pib = pib;
    }
    vector<Odeljenje *> Preduzece::getOdeljenja() const
    {
        return odeljenja;
    }
    void Preduzece::setOdeljenja(vector<Odeljenje *> odeljenja)
    {
        this->odeljenja = odeljenja;
    }
  
    void Preduzece::dodajOdeljenje(Odeljenje *o)
    {
        odeljenja.push_back(o);
    }

    Odeljenje* Preduzece::dobaviOdeljenje(int indeks)
    {
        return odeljenja.at(indeks);
    }
    void Preduzece::brisanje()
    {
        ofstream f;
        f.open("info.txt", ofstream::trunc);
        f.close();
    }
    string Preduzece::tip()
    {
        return "[PREDUZECE]";
    }

    void Preduzece::ispis(ostream &output) const
    {
        vector<Radnik *> svi;
        output << "[PREDUZECE]" << endl << endl;
        output << naziv << " " << maticniBroj << " " << pib << endl << endl;
        output << "[ODELJENJE]" << endl << endl;
        for(Odeljenje *o : odeljenja){
            output << o->getNaziv() << endl << endl;

        }
        for(Odeljenje *o : odeljenja){
            for(Radnik *r : o->getZaposleni()){
                svi.push_back(r);
            }
        }
        output << "[VOZAC]" << endl << endl;

        for(Radnik *r : svi)
            {   
                
                if(r->tip() == "[VOZAC]"){
                    
                    r->ispis(output);
                }
                
            }
        output << "[PRODAVAC]" << endl << endl;
        for(Radnik *r : svi)
            {   
                
                if(r->tip() == "[PRODAVAC]"){
                    
                    r->ispis(output);
                }
                
            }
        output << "[MAGACIONER]" << endl << endl;
        for(Radnik *r : svi)
            {   
                
                if(r->tip() == "[MAGACIONER]"){
                    
                    r->ispis(output);
                }
                
            }

    }
    Preduzece::~Preduzece()
    {
        brisanje();
        for(Odeljenje *o : odeljenja){
            delete o;
            
        }
        
        odeljenja.clear();
        
        cout << "Unisteno preduzece " << naziv << endl;
        
    }