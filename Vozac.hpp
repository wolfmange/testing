#ifndef VOZAC_HPP
#define VOZAC_HPP

#include <vector>
#include "Radnik.hpp"

class Vozac : public Radnik{

    private:
        
        int brojPrekrsaja;
        vector<string> kategorije;
    
    public:
        Vozac();
        Vozac(string ime, string prezime, double visinaPlate, int brojPrekrsaja, Radnik *nadredjeni);
        int getBrojPrekrsaja() const;
        void setBrojPrekrsaja(int brojPrekrsaja);
        void dodajKategoriju(string kategorija);
        string tip();
        void ispis(ostream &output) const;
        void ucitaj(istream &input);
        virtual ~Vozac();
        
};
#endif