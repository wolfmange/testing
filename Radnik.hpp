#ifndef RADNIK_HPP
#define RADNIK_HPP

#include<iostream>
#include<string>
#include<fstream>


using namespace std;

class Radnik{

    private:
        string ime;
        string prezime;
        double visinaPlate;
        Radnik* nadredjeni;
    
    public:
        Radnik();
        Radnik(string ime, string prezime, double visinaPlate, Radnik *nadredjeni);
        string getIme() const;
        void setIme(string ime);
        string getPrezime() const;
        void setPrezime(string prezime);
        double getVisinaPlate() const;
        void setVisinaPlate(double visinaPlate);
        Radnik* getNadredjeni() const;
        void setNadredjeni(Radnik *nadredjeni);
        virtual string tip();
        virtual void ispis(ostream &output) const = 0;
        virtual void ucitaj(istream &input);
        virtual ~Radnik();

};
#endif