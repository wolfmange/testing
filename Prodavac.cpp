#include "Prodavac.hpp"

    Prodavac::Prodavac() : Radnik(){}
    Prodavac::Prodavac(string ime, string prezime, double visinaPlate, int idKase, Radnik *nadredjeni) 
                                                                : Radnik(ime, prezime, visinaPlate, nadredjeni),
                                                                  idKase(idKase){}
    int Prodavac::getIdKase() const
    {
        return idKase;
    }
    void Prodavac::setIdKase(int idKase)
    {
        this->idKase = idKase;
    }

    string Prodavac::tip()
    {
        return "[PRODAVAC]";
    }
    void Prodavac::ispis(ostream &output) const
    {
        Radnik::ispis(output);
        output << idKase << endl << endl;
    }
    void Prodavac::ucitaj(istream &input)
    {
        Radnik::ucitaj(input);
        input >> idKase;       
    }
    Prodavac::~Prodavac(){


        cout << "Unisten prodavac: " << Radnik::getIme() << endl;
    }