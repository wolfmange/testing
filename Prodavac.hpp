#ifndef PRODAVAC_HPP
#define PRODAVAC_HPP

#include "Radnik.hpp"

class Prodavac : public Radnik{

    private:
        int idKase;
    
    public:
        Prodavac();
        Prodavac(string ime, string prezime, double visinaPlate, int idKase, Radnik *nadredjeni);
        int getIdKase() const;
        void setIdKase(int idKase);
        string tip();
        void ispis(ostream &output) const;
        void ucitaj(istream &input);
        virtual ~Prodavac();
};
#endif